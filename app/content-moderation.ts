import axios from 'axios';
import FormData from 'form-data';
import invariant from 'tiny-invariant';

export async function getModerationResults(text: string): Promise<string[]> {
  const moderationResult = await getContentModerationResult(text, 'ml');

  console.log(JSON.stringify(moderationResult));

  let moderationResults: string[] = [];
  if (moderationResult?.moderation_classes?.available.length) {
    moderationResult?.moderation_classes.available.forEach((item: string) => {
      if (moderationResult.moderation_classes[item] > 0.5) {
        moderationResults.push(item);
      }
    });
  }

  return moderationResults;
}

async function getContentModerationResult(text: string, mode: string = 'ml') {
  invariant(process.env.MODERATION_API_USER, "MODERATION_API_USER must be set");
  invariant(process.env.MODERATION_API_SECRET, "MODERATION_API_SECRET must be set");
  invariant(process.env.MODERATION_API_URL, "MODERATION_API_URL must be set");

  const parameters = new FormData();
  parameters.append('text', text);
  parameters.append('lang', 'en');
  parameters.append('mode', mode);
  parameters.append('api_user', process.env.MODERATION_API_USER);
  parameters.append('api_secret', process.env.MODERATION_API_SECRET);

  try {
    const { data: response } = await axios.post(
      process.env.MODERATION_API_URL,
      parameters,
      {
        headers: {
          ...parameters.getHeaders()
        }
      }
    );
    return response;
  } catch (error) {
    console.log(error);
  }
}

/*
curl -X POST 'https://api.sightengine.com/1.0/text/check.json' \
  -F 'text=Don%27t%20be%20a%20silly%20goose%21' \
  -F 'lang=en' \
  -F 'mode=ml' \
  -F 'api_user=abc' \
  -F 'api_secret=def'
*/