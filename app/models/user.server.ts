import type { Role, User } from "@prisma/client";
import bcrypt from "bcryptjs";
import Sqids from "sqids";
import { prisma } from "~/db.server";
import { getRandomInt } from "~/utils";

export type { User } from "@prisma/client";

const userRecordsPerPage = 10;

export async function getUserById(id: User["id"]) {
  const user = await prisma.user.findUnique({
    include: {
      roles: {
        include: {
          role: true
        }
      }
    },
    where: {
      id
    }
  });

  if (user == null) {
    return null;
  }

  let userRoles: UserRoleType[] = []
  user.roles.forEach(userRole => {
    userRoles.push({
      name: userRole.role.name,
      description: userRole.role.description,
    })
  });

  const userWithoutPassword: UserWithoutPassword = {
    id: user.id,
    username: user.username,
    email: user.email,
    userRoles: userRoles
  };

  return userWithoutPassword;
}

export async function getUserByIdUsername(id: User["id"]) {
  const user = await prisma.user.findUnique({
    select: {
      username: true
    },
    where: {
      id
    }
  });

  if (user == null) {
    return null;
  }
  else {
    return user.username;
  }
}

export async function getUserByEmail(email: User["email"]) {
  const user = await prisma.user.findUnique({
    where: {
      email
    }
  });

  if (user == null) {
    return null;
  }

  const userWithoutPassword: UserWithoutPassword = {
    id: user.id,
    username: user.username,
    email: user.email,
    userRoles: null
  };

  return userWithoutPassword;
}

export async function getUserByUsername(username: string) {
  const user = await prisma.user.findUnique({
    where: {
      username
    }
  });

  if (user == null) {
    return null;
  }

  const userWithoutPassword: UserWithoutPassword = {
    id: user.id,
    username: user.username,
    email: user.email,
    userRoles: null
  };

  return userWithoutPassword;
}

export async function createUser(email: User["email"], password: string) {
  const hashedPassword = await bcrypt.hash(password, 10);

  let username: string;

  const sqids = new Sqids();
  do {
    const id = sqids.encode([getRandomInt(1000000000)]);
    username = `user-${id}`;
    const existingUser = await getUserByUsername(username);
    if (existingUser == null) {
      break;
    }
  } while (true);

  return prisma.user.create({
    data: {
      email,
      username,
      passwordHash: hashedPassword,
      createdByUserId: '0'
    },
  });
}

export async function deleteUserByEmail(email: User["email"]) {
  return prisma.user.delete({ where: { email } });
}

export type UserRoleType = {
  name: string;
  description: string | null;
}

export type UserWithoutPassword = {
  id: string;
  username: string | null;
  email: string;
  userRoles: UserRoleType[] | null;
};

export async function verifySignin(
  email: User["email"],
  password: User["passwordHash"],
) {
  const user = await prisma.user.findUnique({
    where: { email },
  });

  if (!user || !user.passwordHash) {
    return null;
  }

  const isValid = await bcrypt.compare(
    password,
    user.passwordHash,
  );

  if (!isValid) {
    return null;
  }

  const userWithoutPassword: UserWithoutPassword = {
    id: user.id,
    username: user.username,
    email: user.email,
    userRoles: null
  };

  return userWithoutPassword;
}

export async function getRoles() {
  return await prisma.role.findMany({
    orderBy: [
      {
        name: 'asc',
      },
    ],
  });
}

export async function getRole(id: Role["id"]) {
  return prisma.role.findUnique({
    where: {
      id
    }
  });
}

export async function getUsers(
  search?: string,
  page: number = 1) {
  if (search == null) {
    search = '';
  }

  if (page == null
    || page < 1) {
    page = 1;
  }

  const skip = (page - 1) * userRecordsPerPage;
  const take = userRecordsPerPage;

  const totalResultsCount = await prisma.user.count({
    where: {
      email: {
        contains: search,
        mode: 'insensitive'
      }
    },
  });

  const totalPageCount = Math.ceil(totalResultsCount / userRecordsPerPage);

  const results = await prisma.user.findMany({
    skip: skip,
    take: take,
    include: {
      roles: {
        include: {
          role: true
        }
      }
    },
    where: {
      email: {
        contains: search,
        mode: 'insensitive'
      }
    },
    orderBy: [
      {
        email: 'asc',
      },
    ],
  });

  return { results, totalPageCount, totalResultsCount };
}

async function getExistingUserRoleAssignment(roleId: string, userId: string) {
  return prisma.userRole.findFirst({
    where: {
      userId,
      roleId
    }
  });
}

export async function addRoleToUser(
  roleId: string,
  userId: string,
  updatedByUserId: string) {

  if (await getExistingUserRoleAssignment(roleId, userId) == null) {
    return prisma.userRole.create({
      data: {
        userId: userId,
        roleId: roleId,
        createdByUserId: updatedByUserId,
      }
    });
  }
}

export async function removeRoleFromUser(
  roleId: string,
  userId: string) {

  const existingUserRole = await getExistingUserRoleAssignment(roleId, userId);
  if (existingUserRole != null) {
    return prisma.userRole.delete({
      where: {
        id: existingUserRole.id
      }
    });
  }

  return null;
}

export async function updateUserEmail(
  id: string,
  email: string,
  updatedByUserId: string
) {
  return prisma.user.update({
    where: {
      id
    },
    data: {
      email,
      updatedByUserId
    },
  })
}

export async function updateUserUsername(
  id: string,
  username: string,
  updatedByUserId: string
) {
  return prisma.user.update({
    where: {
      id
    },
    data: {
      username,
      updatedByUserId
    },
  })
}

export async function updateUserPassword(
  id: string,
  password: string,
  updatedByUserId: string
) {
  const hashedPassword = await bcrypt.hash(password, 10);

  return prisma.user.update({
    where: {
      id
    },
    data: {
      passwordHash: hashedPassword,
      updatedByUserId
    },
  })
}