import type { UserWithoutPassword } from "~/models/user.server";
import { Meme } from "@prisma/client";
import invariant from "tiny-invariant";
import memeSelections from '../meme-selections.json';

const SITE_NAME = 'Meme App';
const DEFAULT_REDIRECT = "/";

export const MIN_CHARACTER_LENGTH = 3;
export const MAX_LENGTH_FOR_INPUT = 500;

export const COMMENT_MAX_CHARACTER_LENGTH = 500;
export const EMAIL_MAX_CHARACTER_LENGTH = 100;
export const TOP_TEXT_MAX_CHARACTER_LENGTH = 100;
export const BOTTOM_TEXT_MAX_CHARACTER_LENGTH = 100;

export const USERNAME_MIN_CHARACTER_LENGTH = 5;
export const USERNAME_MAX_CHARACTER_LENGTH = 30;
export const PASSWORD_MIN_CHARACTER_LENGTH = 8;
export const PASSWORD_MAX_CHARACTER_LENGTH = 50;


/**
 * This should be used any time the redirect path is user-provided
 * (Like the query string on our signin/signup pages). This avoids
 * open-redirect vulnerabilities.
 * @param {string} to The redirect destination
 * @param {string} defaultRedirect The redirect to use if the to is unsafe.
 */
export function safeRedirect(
  to: FormDataEntryValue | string | null | undefined,
  defaultRedirect: string = DEFAULT_REDIRECT,
) {
  if (!to || typeof to !== "string") {
    return defaultRedirect;
  }

  if (!to.startsWith("/") || to.startsWith("//")) {
    return defaultRedirect;
  }

  return to;
}

/**
 * This base hook is used in other hooks to quickly search for specific data
 * across all loader data using useMatches.
 * @param {string} id The route id
 * @returns {JSON|undefined} The router data or undefined if not found
 */
export function useMatchesData(
  id: string,
): Record<string, unknown> | undefined {
  const matchingRoutes = useMatches();
  const route = useMemo(
    () => matchingRoutes.find((route) => route.id === id),
    [matchingRoutes, id],
  );
  return route?.data as Record<string, unknown>;
}

function isUser(user: unknown): user is UserWithoutPassword {
  return (
    user != null &&
    typeof user === "object" &&
    "email" in user &&
    typeof user.email === "string"
  );
}

export function useOptionalUser(): UserWithoutPassword | undefined {
  const data = useMatchesData("root");
  if (!data || !isUser(data.user)) {
    return undefined;
  }
  return data.user;
}

export function useUser(): UserWithoutPassword {
  const maybeUser = useOptionalUser();
  if (!maybeUser) {
    throw new Error(
      "No user found in root loader, but user is required by useUser. If user is optional, try useOptionalUser instead.",
    );
  }
  return maybeUser;
}

export function validateEmail(email: unknown): email is string {
  return typeof email === "string" && email.length > 3 && email.includes("@");
}

export function isValidEmailDomain(email: string): boolean {
  let result = true;
  if (process.env.SIGN_UP_DOMAIN_RESTRICTIONS != null
    && process.env.SIGN_UP_DOMAIN_RESTRICTIONS != '') {
    const signUpDomains = process.env.SIGN_UP_DOMAIN_RESTRICTIONS.split(',');
    const matches = signUpDomains.filter(x => email.trim().toUpperCase().endsWith('@' + x.trim().toUpperCase()));
    result = matches.length > 0;
  }

  return result;
}

export function getTitle(pageTitle: string) {
  return `${pageTitle} - ${SITE_NAME}`;
}

export async function SimulateNetworkDelay() {
  const networkDelay = Number.parseInt(process.env.NETWORK_DELAY as string);
  if (networkDelay != 0) {
    return new Promise((res) => setTimeout(res, networkDelay));
  }
  else {
    return;
  }
}

export function getRandomInt(max: number) {
  return Math.floor(Math.random() * max);
}

export function generateApiMemeImageUrl(
  memeType: string,
  topText: string,
  bottomText: string): string {
  let memeTypeValue = memeType;
  const memeTypeSearch = memeSelections.filter(x => x.slug.toLowerCase() == memeType);
  if (memeTypeSearch.length > 0) {
    memeTypeValue = memeTypeSearch[0].slug;
  }
  return "https://apimeme.com/meme?meme=" +
    encodeURIComponent(memeTypeValue) +
    "&top=" + encodeURIComponent(topText) +
    "&bottom=" + encodeURIComponent(bottomText);
}

export function getMemeImageUrl(
  meme: Pick<Meme, "slug" | "createdDate" | "updatedDate">): string {
  const lastModified = meme.updatedDate != null ? +new Date(meme.updatedDate) : +new Date(meme.createdDate);
  return `/memes/${meme.slug}/image?${lastModified}`;
}

export function checkIfAdmin(user?: UserWithoutPassword) {
  const isAdmin = user?.userRoles?.some(x => x.name === 'Admin');

  return isAdmin;
}

export function checkIfEditable(
  user?: UserWithoutPassword,
  isNew?: boolean,
  active?: boolean,
  createdByUserId?: string) {

  let canEdit = false;
  if (user != null && user != undefined) {
    const isAdmin = checkIfAdmin(user);
    const isCreatedUser = createdByUserId === user.id;
    canEdit = isNew || isAdmin || isCreatedUser;
    if (!isAdmin && active == false) {
      canEdit = false;
    }
  }

  return canEdit;
}

export function checkIfPosted(
  user?: UserWithoutPassword,
  active?: boolean,
  createdByUserId?: string) {

  let isPosted = active === true;
  if (user != null && user != undefined) {
    const isAdmin = checkIfAdmin(user);
    const isCreatedUser = createdByUserId === user.id;
    isPosted = isAdmin || isCreatedUser || active === true;
  }

  return isPosted;
}

export function getPaginationPageNumbers(
  page: number,
  totalPageCount: number,
  pagesToDisplay: number = 5) {

  // Reference: https://gist.github.com/LaffinToo/1544915
  let rangeMiddle = Math.floor(pagesToDisplay / 2);
  let rangeStart = ((page - rangeMiddle) < 1
    ? 1
    : ((page + rangeMiddle) > totalPageCount
      ? (totalPageCount - pagesToDisplay + 1)
      : page - rangeMiddle));
  let rangeEnd = ((page + rangeMiddle) > totalPageCount
    ? totalPageCount
    : (page - rangeMiddle < 1
      ? (pagesToDisplay)
      : page + rangeMiddle));

  rangeStart = Math.max(rangeStart, 1);
  rangeEnd = Math.min(rangeEnd, totalPageCount);

  let pageNumbers: number[] = [];
  for (let i = rangeStart; i <= rangeEnd; i++) {
    pageNumbers.push(i);
  }

  return pageNumbers;
}

// https://byby.dev/js-slugify-string
export function convertTitleToSlug(title: string) {
  let decodedTitle = decodeURIComponent(title);
  return String(decodedTitle)
    .normalize('NFKD') // split accented characters into their base characters and diacritical marks
    .replace(/[\u0300-\u036f]/g, '') // remove all the accents, which happen to be all in the \u03xx UNICODE block.
    .trim() // trim leading or trailing whitespace
    .toLowerCase() // convert to lowercase
    .replace(/[^a-z0-9 -]/g, '') // remove non-alphanumeric characters
    .replace(/\s+/g, '-') // replace spaces with hyphens
    .replace(/-+/g, '-'); // remove consecutive hyphens
};

export function getMemeNameByMemeType(memeType: string) {
  const result = memeSelections.filter(x => convertTitleToSlug(x.name) == memeType);
  if (result.length > 0) {
    return result[0].name;
  }
  else {
    return null;
  }
}

// Reference: https://codepen.io/johano/pen/emNRmb
export function drawMeme(
  canvas: HTMLCanvasElement,
  startImageRef: HTMLImageElement,
  topText: string,
  bottomText: string
) {
  // Initialize the drawing context for the canvas.
  const memeFontName = 'impact';
  let context = canvas.getContext('2d');
  canvas.width = startImageRef.width;
  canvas.height = startImageRef.height;
  invariant(context, 'Context is empty');
  context.clearRect(0, 0, canvas.width, canvas.height);

  // Draw the meme image onto the canvas.
  context.drawImage(startImageRef, 0, 0, startImageRef.width, startImageRef.height);

  // Set the context drawing parameters.
  context.lineJoin = 'round';
  context.miterLimit = 2;
  context.strokeStyle = 'black';
  context.fillStyle = 'white';
  context.textAlign = 'center';
  context.textBaseline = 'top';

  var text1 = topText;
  text1 = text1.toUpperCase();
  // Determine the proportional placement for the 
  // top text to appear horizontally centered and
  // near the top of the image.
  var x = canvas.width / 2;
  var y = canvas.height / 30;

  // Increase the font size until the measured text
  // fills the top section of the image without growing
  // too large.
  var fontSize = 1;
  do {
    context.font = `${fontSize}px ${memeFontName}`;
    var metrics = context.measureText(text1);
    var testWidth = metrics.width;
    fontSize += 1;
  } while (testWidth < canvas.width - (canvas.width / 10)
    && metrics.actualBoundingBoxDescent < canvas.height / 6);

  // Set the line thickness to be proportional to the font size.
  context.lineWidth = 0.15 * fontSize;

  // Draw the top text with a border.
  context.strokeText(text1, x, y);
  context.fillText(text1, x, y);

  context.textBaseline = 'bottom';
  var text2 = bottomText;
  text2 = text2.toUpperCase();
  // Determine the proportional placement for the 
  // bottom text to appear horizontally centered and
  // near the bottom of the image.
  y = canvas.height - (canvas.height / 25);

  // Increase the font size until the measured text
  // fills the bottom section of the image without growing
  // too large.
  var fontSize = 1;
  do {
    context.font = `${fontSize}px ${memeFontName}`;
    var metrics = context.measureText(text2);
    var testWidth = metrics.width;
    fontSize += 1;
  } while (testWidth < canvas.width - (canvas.width / 10)
    && metrics.actualBoundingBoxAscent < canvas.height / 6);

  // Set the line thickness to be proportional to the font size.
  context.lineWidth = 0.15 * fontSize;

  // Draw the bottom text with a border.
  context.strokeText(text2, x, y);
  context.fillText(text2, x, y);

  // Return the encoded JPEG image.
  return getImageData(canvas);
}

export function getImageData(canvas: HTMLCanvasElement) {
  return canvas.toDataURL("image/jpeg", 0.9);
}
