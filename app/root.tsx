import {
  Links,
  Meta,
  Outlet,
  Scripts,
  ScrollRestoration,
  useLoaderData,
  useNavigation,
  useRouteError,
} from "@remix-run/react";

import { getUser } from "~/session.server";
import { Navbar } from "./components/navbar";
import { ToggleTheme } from "./components/toggle-theme";
import { getTitle } from "./utils";
import { Footer } from "./components/footer";

export const loader = async ({
  request,
}: LoaderFunctionArgs) => {
  const user = await getUser(request);
  const enableSignIn = process.env.ENABLE_SIGN_IN === 'true';

  return json({ user, enableSignIn });
};

const noScriptCSS = `
    .noscript {
      display: block !important;
    }
    .hide-noscript {
      display: none !important;
    }`;

export default function App() {
  const { user, enableSignIn } = useLoaderData<typeof loader>();
  const navigation = useNavigation();

  // suppressHydrationWarning is used to suppress client-side color mode console errors.
  // Note for suppressHydrationWarning: https://stackoverflow.com/a/73453215

  return (
    <html lang="en" suppressHydrationWarning>
      <head>
        <noscript>
          <style>
            {noScriptCSS}
          </style>
        </noscript>
        <script src="/color-modes.js"></script>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1" />
        <Meta />
        <Links />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossOrigin="anonymous"></link>
        <link rel="stylesheet" href="/styles.css"></link>
      </head>
      <body
        className={
          navigation.state === "loading" ? "loading bg-light-subtle" : "bg-light-subtle"
        }>
        <Navbar user={user} enableSignIn={enableSignIn} />

        <div className="col-lg-10 mx-auto p-4 py-md-5 shadow p-3 mb-5 bg-body-tertiary rounded">
          <Outlet />
        </div>

        <ScrollRestoration />
        <Scripts />
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossOrigin="anonymous"></script>
        <ToggleTheme />

        <Footer />
      </body>
    </html>
  );
}

export function ErrorBoundary() {
  const error = useRouteError();
  console.error(error);

  // suppressHydrationWarning is used to suppress client-side color mode console errors.
  // Note for suppressHydrationWarning: https://stackoverflow.com/a/73453215

  return (
    <html lang="en" suppressHydrationWarning>
      <head>
        <noscript>
          <style>
            {noScriptCSS}
          </style>
        </noscript>
        <script src="/color-modes.js"></script>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1" />
        <title>{error?.status == 404 ? getTitle('Page Not Found') : getTitle('Error')}</title>
        <Links />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossOrigin="anonymous"></link>
        <link rel="stylesheet" href="/styles.css"></link>
      </head>
      <body className="bg-light-subtle">

        <div className="col-lg-10 mx-auto p-4 py-md-5 shadow p-3 mb-5 bg-body-tertiary rounded">

          {error?.status == 404 ? (
            <>
              <h2>Page not found! 😿</h2>
              <br />
              <p>
                That page 🖼️ was not found in the Meme App!
              </p>
            </>
          ) : (
            <>
              <h2>Oh no! 🙀</h2>
              <br />
              <p>
                An error 💥 just occurred in the Meme App!
              </p>
              So sorry 😿 about that.  We will try to fix it 👷 soon. 
            </>
          )}

          <hr />
          <a href="/">Go to the home page</a>
        </div>

        <ScrollRestoration />
        <Scripts />
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossOrigin="anonymous"></script>
        <ToggleTheme />

        <Footer />
      </body>
    </html>
  );
}