import { Meme } from "@prisma/client";
import { CommentErrors } from "~/edit-comment.server";
import { CommentResult } from "~/models/comment.server";
import { MAX_LENGTH_FOR_INPUT } from "~/utils";

export function EditComment({
  comment,
  meme,
  errors
}: {
  comment: Pick<CommentResult, "id" | "text" | "memeSlug">;
  meme: Meme;
  errors?: CommentErrors | undefined;
}) {
  const navigation = useNavigation();
  const isSubmitting = Boolean(
    navigation.state === "submitting"
  );

  const isUpdateAction = navigation.formData
    ? navigation.formData.get("update") === "true" && isSubmitting
    : false;

  if (errors?.originalComment != null) {
    comment.text = errors.originalComment
  }

  const commentRef = useRef<HTMLTextAreaElement>(null);

  useEffect(() => {
    commentRef.current?.focus();
  }, [comment.text]);

  return (
    <>
      <Form method="post">
        <h2>
          {comment.id != '' ? (
            <>Edit </>
          ) : (
            <>Add </>
          )}
          Comment for {meme.topText} {meme.bottomText}
        </h2>
        <div className="container">
          <div className="row g-3">
            <div className="col-md-5">
              <div>
                <label htmlFor="comment" className="form-label">Comment</label>
                <div className="col-sm-10 input-group has-validation">
                  <textarea ref={commentRef} id="comment" className="form-control" name="comment" rows={3} defaultValue={comment?.text} maxLength={MAX_LENGTH_FOR_INPUT} />
                  {errors?.comment ? (
                    <div className="custom-invalid-feedback">{errors.comment}</div>
                  ) : null}
                </div>
              </div>
              <div>
                <Link
                  to={`/memes/${meme.slug}?commentId=${comment.id}`}
                  reloadDocument
                  className="btn btn-sm btn-secondary"
                >
                  Cancel
                </Link>
                &nbsp;
                <button className="btn btn-sm btn-primary" type="submit" name="update" value="true" disabled={isUpdateAction}>
                  {isUpdateAction ? (
                    <span className="spinner-border spinner-border-sm" aria-hidden="true"></span>)
                    : null}
                  {comment.id != '' ? (
                    <>{isUpdateAction ? 'Updating Comment...' : "Update Comment"}</>
                  ) : (
                    <>{isUpdateAction ? 'Adding Comment...' : "Add Comment"}</>
                  )}
                </button>
              </div>
            </div>
          </div>
          {comment.id != '' ? (
            <>
              <br />
              <div className="row g-3">
                <div className="col-md-5">
                  <Link
                    to={`/memes/comment/${comment.id}/remove`}
                    className="btn btn-sm btn-danger"
                  >
                    Remove
                  </Link>
                </div>
              </div>
            </>
          ) : null}
        </div>
        <input type="hidden" name="id" value={comment.id} />
        <input type="hidden" name="memeSlug" value={meme.slug} />
      </Form>
    </>
  );
};