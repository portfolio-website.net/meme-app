import invariant from "tiny-invariant";
import { drawMeme, convertTitleToSlug, getImageData, getMemeNameByMemeType } from "~/utils";
import { detectFont } from 'detect-font';

export function RenderImage({
  slug,
  memeType,
  topText,
  bottomText,
  saveImage,
  onDraw
}: {
  slug?: string;
  memeType: string;
  memeName: string;
  topText: string;
  bottomText: string;
  saveImage: boolean;
  onDraw: any;
}) {
  const fetcher = useFetcher();

  function refreshMeme() {
    const imageData = drawMeme(
      canvasRef.current as HTMLCanvasElement,
      startImageRef.current as HTMLImageElement,
      topText,
      bottomText);

    setImageData(imageData);
    if (imageDataRef.current) {
      imageDataRef.current.value = imageData;
    }

    invariant(memeFontRef.current, 'Meme font block is empty');
    const detectedFont = detectFont(memeFontRef.current);

    if (detectedFont === 'impact') {
      if (saveImage) {
        invariant(submitButtonRef.current, 'submitButtonRef is empty');
        submitButtonRef.current.click();
      }

      if (onDraw != null) {
        onDraw(getImageData(canvasRef.current as HTMLCanvasElement));
      }
    }
    else {
      if (saveImage) {
        setMemeFontUnavailableMessageHidden(false);
      }

      if (onDraw != null) {
        onDraw(null);
        setMemeFontUnavailableMessageHidden(false);
      }
    }
  }

  const canvasRef = useRef<HTMLCanvasElement>(null);
  const startImageRef = useRef<HTMLImageElement>(null);
  const imageDataRef = useRef<HTMLInputElement>(null);
  const formRef = useRef<HTMLFormElement>(null);
  const submitButtonRef = useRef<HTMLButtonElement>(null);
  const memeFontRef = useRef<HTMLDivElement>(null);

  const [memeFontUnavailableMessageHidden, setMemeFontUnavailableMessageHidden] = useState(true);
  const [startImageUrl, setStartImageUrl] = useState('');
  const [imageData, setImageData] = useState('');

  useEffect(() => {
    if (memeType != null
    ) {
      setStartImageUrl(`/images/memes/${convertTitleToSlug(memeType)}.jpg`);
      invariant(startImageRef.current, 'startImage is empty');
      startImageRef.current.src = `/images/memes/${convertTitleToSlug(memeType)}.jpg`;
    }
  }, [memeType, topText, bottomText]);

  if (saveImage) {
    const memeName = getMemeNameByMemeType(memeType);

    return (
      <>
        <fetcher.Form
          style={{ minHeight: '150px' }}
          action="/memes/render"
          method="post"
          ref={formRef}>
          <canvas ref={canvasRef} className="card-img-top" style={{ maxWidth: '150px' }}>
            Sorry, images cannot be rendered.  Please try again with a different browser.
          </canvas>
          <div ref={memeFontRef} hidden={true} style={{ fontFamily: 'impact' }}>
            Impact Example Text
          </div>
          <div hidden={memeFontUnavailableMessageHidden} className="alert alert-secondary" role="alert">
            Sorry, the meme font cannot be rendered with this browser.  Please try again with a different browser.
          </div>
          <img className="hidden" alt="" ref={startImageRef} src={startImageUrl} onLoad={refreshMeme} />
          <input type="hidden" ref={imageDataRef} name="imageData" value={imageData} />
          <input type="hidden" name="slug" value={slug} />
          <button className="hidden" type="submit" ref={submitButtonRef}></button>
        </fetcher.Form>
        <div>
          <span className="badge rounded-pill text-bg-secondary">{memeName}</span>
        </div>
      </>
    );
  }
  else {
    return (
      <>
        <canvas className="card-img-top" ref={canvasRef}>
          Sorry, images cannot be rendered.  Please select a meme type and save to continue.
        </canvas>
        <div ref={memeFontRef} hidden={true} style={{ fontFamily: 'impact' }}>
          Impact Example Text
        </div>
        <div hidden={memeFontUnavailableMessageHidden} className="alert alert-secondary" role="alert">
          Sorry, the meme font cannot be rendered with this browser.  The meme image will be automatically regenerated when saved.
        </div>
        <img className="hidden" alt="" ref={startImageRef} src={startImageUrl} onLoad={refreshMeme} />
      </>
    );
  }
}