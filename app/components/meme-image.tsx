import { getMemeImageUrl, getMemeNameByMemeType } from "~/utils";
import { VoteButton } from "./vote-button";
import { UserWithoutPassword } from "~/models/user.server";

export function MemeImage({
  meme,
  user,
  renderLink,
  voteRedirectTo
}: {
  meme: any;
  user: UserWithoutPassword;
  renderLink: boolean;
  voteRedirectTo: string
}) {
  const memeName = getMemeNameByMemeType(meme.memeType);

  return (

    <div className="card">
      <div className="image">
        <div className="overlay">
          <VoteButton meme={{ slug: meme.slug, voted: meme.vote.length != 0, voteCount: meme._count.vote }} user={user} redirectTo={voteRedirectTo} />
        </div>
        {renderLink ? (
          <Link
            to={`/memes/${meme.slug}`}>
            <img className="card-img-top" src={getMemeImageUrl(meme)} alt={`${meme.topText} ${meme.bottomText}`} />
          </Link>
        ) : (
          <img className="card-img-top" src={getMemeImageUrl(meme)} alt={`${meme.topText} ${meme.bottomText}`} />
        )}

      </div>
      <div className="card-body">
        <h5 className="card-title">{meme.topText} {meme.bottomText}</h5>
        <div className="card-text">
          <div className="text-center">
            <span className="badge rounded-pill text-bg-secondary">{memeName}</span>
          </div>
          {meme.active == false ?
            (
              meme.moderationComment != null ? (
                <div className="alert alert-danger" role="alert">
                  <strong>Moderation comment:</strong> {meme.moderationComment}
                  <br />The meme is not displayed publicly.
                </div>
              ) : (
                <div className="alert alert-warning" role="alert">
                  This meme is pending moderation and not displayed publicly.
                </div>
              )
            )
            : null}
        </div>
      </div>
    </div>
  );
}