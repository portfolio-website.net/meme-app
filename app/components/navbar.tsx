import { UserWithoutPassword } from "~/models/user.server";
import { checkIfAdmin } from "~/utils";

export type NavbarLink = {
    title?: string;
    link?: string;
    isAdminLink: boolean
};

const links = {
    records: {} as Record<string, NavbarLink>,

    getAll(): NavbarLink[] {
        return Object.keys(links.records)
            .map((key) => links.records[key]);
    },

    async create(values: NavbarLink): Promise<NavbarLink> {
        const newLink = { ...values };
        links.records[values.title?.toString() ?? ''] = values;
        return newLink;
    },
};

[
    {
        title: "Home",
        link: "/",
        isAdminLink: false
    },
    {
        title: "Moderation",
        link: "/memes/moderation",
        isAdminLink: true
    },
    {
        title: "Batch Render",
        link: "/batch-render",
        isAdminLink: true
    },
].forEach((item) => {
    links.create({
        ...item
    });
});

export function Navbar({
    user,
    search,
    enableSignIn
}: {
    user?: UserWithoutPassword;
    search?: string;
    enableSignIn: boolean;
}) {
    const isAdmin = checkIfAdmin(user);
    const navbarLinks = links.getAll()
        .filter((item) => item.isAdminLink == false || (item.isAdminLink == isAdmin));

    const navigation = useNavigation();

    return (
        <nav className="navbar navbar-expand-lg shadow p-3 mb-4 bg-body-tertiary border-bottom">
            <div className="container-fluid">
                <div
                    style={{ visibility: (navigation.state === "loading") ? "visible" : "hidden" }}
                    id="navbar-spinner" className="spinner-border" role="status">
                    <span className="visually-hidden">Loading...</span>
                </div>
                &nbsp;
                <NavLink to="/" className="navbar-brand"><img src="/meme-app.png" alt="Meme App" /> Meme App</NavLink>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                        {navbarLinks.map((link) => (
                            <li key={link.title} className="nav-item nowrap">
                                <NavLink
                                    className={({ isActive, isPending }) =>
                                        isActive
                                            ? "nav-link active"
                                            : isPending
                                                ? "nav-link pending"
                                                : "nav-link"
                                    }
                                    to={link.link} end
                                >
                                    {link.title}
                                </NavLink>
                            </li>
                        ))}
                        {user?.id != null ? (
                            <>
                                <li className="nav-item">
                                    <NavLink to="/memes/new" className="btn btn-outline-primary nowrap">
                                        New Meme
                                    </NavLink>
                                </li>
                                <li className="nav-item dropdown hide-noscript">
                                    <a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                        {user?.email}
                                    </a>
                                    <ul className="dropdown-menu">
                                        <li><NavLink className="dropdown-item" to="/profile" reloadDocument>Profile</NavLink></li>
                                        {isAdmin ? (
                                            <li><NavLink className="dropdown-item" to="/user-management" reloadDocument>User Management</NavLink></li>
                                        ) : null}
                                    </ul>
                                </li>
                                <li className="nav-item py-2 py-lg-1 col-12 col-lg-auto hidden noscript">
                                    <div className="vr d-none d-lg-flex h-100 mx-lg-2 text-white"></div>
                                    <hr className="d-lg-none my-2 text-white-50" />
                                </li>
                                <li className="nav-item hidden noscript">{user?.email}</li>
                                <li className="nav-item py-2 py-lg-1 col-12 col-lg-auto hidden noscript">
                                    <div className="vr d-none d-lg-flex h-100 mx-lg-2 text-white"></div>
                                    <hr className="d-lg-none my-2 text-white-50" />
                                </li>
                                <li className="nav-item noscript hidden noscript"><NavLink to="/profile">Profile</NavLink></li>
                                <li className="nav-item py-2 py-lg-1 col-12 col-lg-auto hidden noscript">
                                    <div className="vr d-none d-lg-flex h-100 mx-lg-2 text-white"></div>
                                    <hr className="d-lg-none my-2 text-white-50" />
                                </li>
                                {isAdmin ? (
                                    <li className="nav-item nowrap hidden noscript"><NavLink to="/user-management">User Management</NavLink></li>
                                ) : null}
                                <li className="nav-item py-2 py-lg-1 col-12 col-lg-auto">
                                    <div className="vr d-none d-lg-flex h-100 mx-lg-2 text-white"></div>
                                    <hr className="d-lg-none my-2 text-white-50" />
                                </li>
                                <li>
                                    <Form className="nav-item" role="signout" action="/signout" method="post">
                                        <button type="submit" className="btn btn-outline-secondary nowrap">Sign out</button>
                                    </Form>
                                </li>
                            </>
                        ) : (
                            <>
                                {enableSignIn ? (
                                    <li className="nav-item">
                                        <NavLink to="/signin" className="btn btn-outline-secondary">Sign in</NavLink>
                                    </li>
                                ) : null}
                            </>
                        )}
                    </ul>
                    <Form
                        className="d-flex"
                        role="search"
                        action="/">
                        <input
                            className="form-control me-2"
                            type="search"
                            name="search"
                            placeholder="Search"
                            aria-label="Search"
                            defaultValue={search || ""}
                        />
                        <button className="btn btn-outline-success" type="submit">Search</button>
                    </Form>
                </div>
            </div>
        </nav>

    );
}