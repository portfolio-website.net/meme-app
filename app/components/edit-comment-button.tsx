import { CommentResult } from "~/models/comment.server";
import { UserWithoutPassword } from "~/models/user.server";
import { MAX_LENGTH_FOR_INPUT, checkIfEditable } from "~/utils";

export function EditCommentButton({
  comment,
  user
}: {
  comment: CommentResult;
  user: UserWithoutPassword;
}) {
  const navigation = useNavigation();
  const isSubmitting = Boolean(
    navigation.state === "submitting"
  );

  let isUpdateAction = navigation.formData
    ? navigation.formData?.get("update") === "true" && isSubmitting
    : false;

  const canEdit = checkIfEditable(user, false, comment?.active, comment?.createdByUserId);

  if (user != null) {
    return (
      <div className="container-fluid">
        <Form
          method="post"
          action={`/memes/${comment.memeSlug}/comment/new`}>

          {comment?.id != '' && canEdit ? (
            <>
              <Link
                to={`/memes/comment/${comment.id}/edit`}
                reloadDocument
                className="btn btn-secondary btn-sm"
              >
                Edit
              </Link>
            </>
          ) : comment?.id == '' ? (
            <div>
              <div className="row">
                <label htmlFor="comment" className="form-label">Comment</label>
                <div className="col-sm-10 input-group has-validation">
                  <textarea id="comment" className="form-control" name="comment" rows={3} defaultValue={comment?.text} maxLength={MAX_LENGTH_FOR_INPUT} />
                </div>
              </div>
              <div className="col-12">
                <button className="btn btn-sm btn-primary" type="submit" name="update" value="true" disabled={isUpdateAction}>
                  {isUpdateAction ? (
                    <span className="spinner-border spinner-border-sm" aria-hidden="true"></span>)
                    : null}
                  {isUpdateAction ? 'Adding Comment...' : "Add Comment"}
                </button>
              </div>
            </div>
          ) : null}

          <input type="hidden" name="memeSlug" value={comment.memeSlug} />
        </Form>
      </div>
    );
  }
  else {
    return null;
  }
};