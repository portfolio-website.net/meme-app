import { Meme } from "@prisma/client";
import { MemeErrors } from "~/edit-meme.server";
import { MemeSelector } from "./meme-selector";
import { MemeSelectorModal } from "./meme-selector-modal";
import { RenderImage } from "./render-image";
import { MAX_LENGTH_FOR_INPUT } from "~/utils";

export function EditMeme({
  meme,
  search,
  page,
  errors
}: {
  meme: Pick<Meme, "id" | "slug" | "memeType" | "topText" | "bottomText">;
  search: string;
  page: number;
  errors?: MemeErrors | undefined;
}) {
  const navigation = useNavigation();
  const isSubmitting = Boolean(
    navigation.state === "submitting"
  );

  const [topText, setTopText] = useState(meme.topText);
  const [bottomText, setBottomText] = useState(meme.bottomText);
  const [memeType, setMemeType] = useState(meme.memeType);
  const [imageData, setImageData] = useState('');

  function topTextChange(e: any) {
    const topText = e.currentTarget.value;
    setTopText(topText);
  }

  function bottomTextChange(e: any) {
    const bottomText = e.currentTarget.value;
    setBottomText(bottomText);
  }

  function updateMemeImage(value: string) {
    setMemeType(value);
  }

  function onDraw(imageData: string) {
    setImageData(imageData);
  }

  useEffect(() => {
    const memeTypeField = document.getElementById("memeType");
    if (memeTypeField instanceof HTMLSelectElement) {
      memeTypeField.value = memeType || "";
    }
  }, [memeType]);

  const modalId = `modal-memeSelector`;

  return (
    <div className="container">
      {meme.slug != '' ? (
        <>
          <Link
            to={`/memes/${meme.slug}`}
            className="btn btn-secondary"
          >
            View
          </Link>
          &nbsp;
          <Link
            to={`/memes/${meme.slug}/remove`}
            className="btn btn-danger"
          >
            Remove
          </Link>
        </>
      ) : null}

      <Form method="post" className="row g-3">
        <div className="col-md-6">
          <label htmlFor="memeType" className="form-label">Meme Type</label>
          <div className="col-sm-10 input-group has-validation">
            <MemeSelector memeType={memeType} modalId={modalId} onChange={updateMemeImage} />
            {errors?.memeType ? (
              <div className="custom-invalid-feedback">{errors.memeType}</div>
            ) : null}
          </div>
          <div>
            <br />
            <input type="hidden" name="imageData" value={imageData} />
            <RenderImage memeType={memeType} topText={topText} bottomText={bottomText} saveImage={false} onDraw={onDraw} />
          </div>
        </div>
        <div className="col-md-6">
          <div>
            <label htmlFor="topText" className="form-label">Top Text</label>
            <div className="col-sm-10 input-group has-validation">
              <input type="text" className="form-control" id="topText" name="topText" defaultValue={meme?.topText} required onChange={topTextChange} maxLength={MAX_LENGTH_FOR_INPUT} />
              {errors?.topText ? (
                <div className="custom-invalid-feedback">{errors.topText}</div>
              ) : null}
            </div>
          </div>
          <div>
            <label htmlFor="bottomText" className="form-label">Bottom Text</label>
            <div className="col-sm-10 input-group has-validation">
              <input type="text" className="form-control" id="bottomText" name="bottomText" defaultValue={meme?.bottomText} required onChange={bottomTextChange} maxLength={MAX_LENGTH_FOR_INPUT} />
              {errors?.bottomText ? (
                <div className="custom-invalid-feedback">{errors.bottomText}</div>
              ) : null}
            </div>
          </div>
          <div>
            <br />
            <input type="hidden" name="id" value={meme.id} />
            <input type="hidden" name="update" value="true" />
            <button className="btn btn-primary" type="submit" disabled={isSubmitting}>
              {isSubmitting ? (
                <span className="spinner-border spinner-border-sm" aria-hidden="true"></span>)
                : null}
              {isSubmitting ? 'Saving Meme...' : "Save Meme"}
            </button>
          </div>
        </div>
      </Form>

      <MemeSelectorModal modalId={modalId} search={search} page={page} topText={topText} bottomText={bottomText} onChange={updateMemeImage} />
    </div>
  );
};