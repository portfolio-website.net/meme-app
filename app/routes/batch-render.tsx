import invariant from "tiny-invariant";
import { RenderImage } from "~/components/render-image";
import { getNextUnrenderedMeme } from "~/models/meme.server";
import { requireUser } from "~/session.server";
import { checkIfAdmin, getTitle } from "~/utils";

export const meta: MetaFunction = () => [{ title: getTitle('Batch Render') }];

export const loader = async ({
  request,
}: LoaderFunctionArgs) => {
  const user = await requireUser(request);
  const isAdmin = checkIfAdmin(user);

  invariant(isAdmin, 'User must be an admin.');

  const { results, totalResultsCount } = await getNextUnrenderedMeme();

  return json({ results, totalResultsCount });
};

export default function BatchRender() {
  const { results, totalResultsCount } = useLoaderData<typeof loader>();

  const meme = results[0];

  if (results.length) {
    return (
      <div>
        <>
          <div className="alert alert-primary" role="alert">
            Rendering image for {results[0].topText} {results[0].bottomText}... {' '}
            <span className="spinner-border spinner-border-sm" aria-hidden="true"></span>
            <hr />
            <RenderImage slug={meme.slug} memeType={meme.memeType} topText={meme.topText} bottomText={meme.bottomText} saveImage={true} />
            <hr />
            Images remaining: {totalResultsCount.toLocaleString("en-US")}
          </div>
        </>
      </div>
    );
  }
  else {
    return (
      <div className="alert alert-success" role="alert">
        Processing Complete - All meme images rendered! ✨
      </div>
    );
  }
}




