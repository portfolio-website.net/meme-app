import { Comment } from "@prisma/client";
import invariant from "tiny-invariant";
import { getAllModerationComments, updateCommentModerationAction } from "~/models/comment.server";
import { requireUser } from "~/session.server";
import { useDebounceSubmit } from "remix-utils/use-debounce-submit";
import { Pagination } from "~/components/pagination";
import { SimulateNetworkDelay, checkIfAdmin, getTitle } from "~/utils";
import { ModerationTabs } from "~/components/moderation-tabs";

export const meta: MetaFunction = () => [{ title: getTitle('Moderation - Comments') }];

export const loader = async ({ request }: LoaderFunctionArgs) => {
  let user = await requireUser(request);
  const isAdmin = checkIfAdmin(user);

  invariant(isAdmin, 'User must be an admin.');

  const url = new URL(request.url);
  let search = url.searchParams.get("search") as string;

  if (search == null) {
    search = '';
  }

  let page = Number(url.searchParams.get("page"));

  if (page == 0) {
    page = 1;
  }

  const { results, totalPageCount, totalResultsCount } = await getAllModerationComments(search, page);

  return json({ results, search, page, totalPageCount, totalResultsCount });
};

export const action = async ({
  params,
  request,
}: ActionFunctionArgs) => {
  await SimulateNetworkDelay();

  const user = await requireUser(request);
  const isAdmin = checkIfAdmin(user);

  invariant(isAdmin, 'User must be an admin.');

  const formData = await request.formData();
  const id = formData.get("id")?.toString();
  invariant(id, "Missing id param");

  await updateCommentModerationAction({
    id: id,
    approve: formData.get("approve") === "true",
    moderationComment: formData.get("moderationComment")?.toString(),
    updatedByUserId: user.id
  });

  return redirectDocument(request.url);
};

export default function ViewCommentModerationQueue() {
  const { results, search, page, totalPageCount, totalResultsCount } = useLoaderData<typeof loader>();

  const navigation = useNavigation();
  // https://sergiodxa.github.io/remix-utils/#md:debounced-fetcher-and-submit
  const submit = useDebounceSubmit();
  const searching =
    navigation.location &&
    new URLSearchParams(navigation.location.search).has(
      "search"
    );

  useEffect(() => {
    const searchField = document.getElementById("search");
    if (searchField instanceof HTMLInputElement) {
      searchField.value = search || "";
    }
  }, [search]);

  return (
    <div className="container">
      <h2>
        Moderation Queue
      </h2>

      <ModerationTabs />

      <Form
        id="search-form"
        onChange={(event) => {
          const isFirstSearch = search === null;
          submit(event.currentTarget, {
            debounceTimeout: 1000,
            replace: !isFirstSearch,
            preventScrollReset: true,
          });
        }}
        role="search"
      >
        <div className="input-group mb-3">
          <input
            aria-label="Search comments"
            className={searching ? "loading form-control search" : "form-control search"}
            defaultValue={search || ""}
            id="search"
            name="search"
            placeholder="Search"
            type="search"
          />
          <div
            hidden={!searching}
            className="spinner-border spinner-border-sm search-spinner"
            role="status">
            <span className="visually-hidden">Loading...</span>
          </div>
          <button className="btn btn-success search" type="submit">Search</button>
          <Pagination search={search} useSearch={true} page={page} totalPageCount={totalPageCount} />
        </div>
      </Form>

      <strong>Total Comments:</strong> {totalResultsCount.toLocaleString("en-US")}

      {results.length > 0 ? (
        <table className="table table-striped table-hover">
          <thead>
            <tr>
              <th>Meme</th>
              <th>Comment</th>
              <th>Outcome</th>
              <th className="nowrap">Moderation Result</th>
              <th>Comment</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {results.map((comment) => (
              <tr key={comment.id}>
                <td>
                  {comment.meme.topText} {comment.meme.bottomText}
                </td>
                <td style={{ whiteSpace: 'pre-line' }}>
                  <Link
                    to={`/memes/${comment.memeSlug}?commentId=${comment.id}`}
                    reloadDocument
                  >
                    {comment.text}
                  </Link>
                </td>
                <td>
                  {comment.active == true ? (
                    <span>Approved</span>
                  ) : (comment.active == false ? (
                    <span>Rejected</span>
                  ) : null)}
                </td>
                <td>
                  {comment.moderationResult}
                </td>
                <td>
                  {comment.moderationComment}
                </td>
                <td>
                  <ApproveButton comment={comment} search={search} page={page} />
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      ) : (
        <div className="alert alert-secondary" role="alert">
          No comments found.
        </div>
      )}

      <Pagination search={search} useSearch={true} page={page} totalPageCount={totalPageCount} />
    </div>
  );
}

export function ApproveButton({
  comment,
  search,
  page
}: {
  comment: Comment;
  search: string;
  page: number;
}) {
  const fetcher = useFetcher();
  const approve = fetcher.formData
    ? fetcher.formData.get("approve") === "true"
    : comment.active;

  const displayApproveAction = !approve;

  return (
    <fetcher.Form method="post">
      <input type="hidden" name="id" value={comment.id} />

      {displayApproveAction ? (
        <>
          <button
            className="btn btn-success btn-sm"
            name="approve"
            value="true"
          >
            Approve
          </button>
          &nbsp;
        </>
      ) : null}

      {!displayApproveAction
        || comment.moderationComment == null
        || comment.moderationComment == '' ?
        (
          <Link
            className="btn btn-secondary btn-sm"
            to={`/memes/moderation/comments/${comment.id}/reject?search=${search}&page=${page}`}
          >
            Reject
          </Link>
        ) : null}

    </fetcher.Form>
  );
};