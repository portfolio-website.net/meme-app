import { signout } from "~/session.server";

export const action = async ({ request }: ActionFunctionArgs) =>
  signout(request);

export const loader = async () => redirect("/");
