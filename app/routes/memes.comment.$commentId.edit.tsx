import { getMeme } from "~/models/meme.server";
import invariant from "tiny-invariant";
import { requireUserId } from "~/session.server";
import { SimulateNetworkDelay, getTitle } from "~/utils";
import { getComment } from "~/models/comment.server";
import { CommentErrors, editCommentAction } from "~/edit-comment.server";
import { EditComment } from "~/components/edit-comment";

export const meta: MetaFunction<typeof loader> = ({
  data
}) => {
  invariant(data, "data is required");
  return [{ title: getTitle(`Edit Comment for ${data.meme.topText} ${data.meme.bottomText}`) }];
};

export const loader = async ({
  params,
  request
}: LoaderFunctionArgs) => {
  const userId = await requireUserId(request);

  invariant(params.commentId, "params.commentId is required");

  const comment = await getComment(params.commentId);
  invariant(comment, `Comment not found: ${params.commentId}`);

  const meme = await getMeme(comment.memeSlug, userId);
  
  if (!meme) {
    throw new Response(null, {
      status: 404,
      statusText: "Not Found",
    });
  }

  return json({ comment, meme });
};

export const action = async ({
  request,
}: ActionFunctionArgs) => {
  await SimulateNetworkDelay();

  return await editCommentAction(request);
};

export default function EditCommentResponse() {
  const { comment, meme } = useLoaderData<typeof loader>();
  const errors = useActionData<typeof action>();

  const commentErrors: CommentErrors = {
    comment: errors?.comment,
    moderationResult: errors?.moderationResult,
  };

  return (
    <EditComment comment={{
      id: comment.id,
      text: comment.text,
      memeSlug: comment.memeSlug,
    }}
      meme={meme}
      errors={commentErrors} />
  );
}
