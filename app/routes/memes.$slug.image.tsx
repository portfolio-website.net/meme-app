import { createCanvas, loadImage } from 'canvas';
import invariant from "tiny-invariant";
import { getMemeWithImageData } from "~/models/meme.server";
import { generateApiMemeImageUrl } from "~/utils";

export async function generateImage(
  url: string
) {
  const background = await loadImage(url);
  const width = background.width;
  const height = background.height;

  const canvas = createCanvas(width, height);
  const ctx = canvas.getContext('2d');

  ctx.drawImage(background, 0, 0, width, height);

  const image = canvas.toBuffer('image/jpeg');
  return image;
}

export const loader = async ({
  params,
  request,
}: LoaderFunctionArgs) => {
  invariant(params.slug, "params.slug is required");

  const meme = await getMemeWithImageData(params.slug);
  
  if (!meme) {
    throw new Response(null, {
      status: 404,
      statusText: "Not Found",
    });
  }

  if (meme.imageData != null && meme.imageData != '') {
    const binaryBuffer = Buffer.from(meme.imageData.replace('data:image/jpeg;base64,', ''), 'base64');

    return new Response(binaryBuffer, {
      headers: {
        'Content-Type': 'image/jpeg',
        'Last-Modified': meme.updatedDate?.toUTCString() ?? meme.createdDate?.toUTCString()
      }
    });
  }
  else {
    const memeImageUrl = generateApiMemeImageUrl(meme.memeType, meme.topText, meme.bottomText);    
    const image = await generateImage(memeImageUrl);

    return new Response(image, {
      headers: {
        'Content-Type': 'image/jpeg',
      },
    });
  }
};
