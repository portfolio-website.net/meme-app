import { getTitle, useOptionalUser } from "~/utils";
import { USER_NEW_SIGNUP_KEY, commitSession, getSession, getUserId } from "~/session.server";
import { getMemes } from "~/models/meme.server";
import { MemeImage } from "~/components/meme-image";
import { Pagination } from "~/components/pagination";
import { useDebounceSubmit } from "remix-utils/use-debounce-submit";

export const meta: MetaFunction = () => [{ title: getTitle('Home') }];

export const loader = async ({
  request,
}: LoaderFunctionArgs) => {
  const userId = await getUserId(request);

  const url = new URL(request.url);
  let search = url.searchParams.get("search") as string;

  if (search == null) {
    search = '';
  }

  let page = Number(url.searchParams.get("page"));

  if (page == 0) {
    page = 1;
  }

  const { results, totalPageCount, totalResultsCount } = await getMemes(userId, search, false, page);

  const session = await getSession(request);
  const newSignUp = session.get(USER_NEW_SIGNUP_KEY) || null;

  if (newSignUp == true) {
    return redirect(
      '/signupwelcome',
      {
        headers: {
          'Set-Cookie': await commitSession(session),
        },
      },
    );
  }

  return json(
    { results, search, page, totalPageCount, totalResultsCount },
    {
      headers: {
        'Set-Cookie': await commitSession(session),
      },
    },
  );
};

export default function Index() {
  const user = useOptionalUser();

  const { results, search, page, totalPageCount, totalResultsCount } = useLoaderData<typeof loader>();

  const navigation = useNavigation();
  // https://sergiodxa.github.io/remix-utils/#md:debounced-fetcher-and-submit
  const submit = useDebounceSubmit();
  const searching =
    navigation.location &&
    new URLSearchParams(navigation.location.search).has(
      "search"
    );

  useEffect(() => {
    const searchField = document.getElementById("search");
    if (searchField instanceof HTMLInputElement) {
      searchField.value = search || "";
    }
  }, [search]);

  const voteRedirectTo = `/?search=${search}&page=${page}`;

  return (
    <div className="container">

      {user != null ? (
        <>
          <Link to="/memes/new" className="btn btn-primary">
            New Meme
          </Link>
          <br />
          <br />
        </>
      ) : null}

      <strong>Total Memes:</strong> {totalResultsCount.toLocaleString("en-US")}
      <Form
        id="search-form"
        onChange={(event) => {
          const isFirstSearch = search === null;
          submit(event.currentTarget, {
            debounceTimeout: 1000,
            replace: !isFirstSearch,
            preventScrollReset: true,
          });
        }}
        role="search"
      >
        <div className="input-group mb-3">
          <input
            aria-label="Search memes"
            className={searching ? "loading form-control search" : "form-control search"}
            defaultValue={search || ""}
            id="search"
            name="search"
            placeholder="Search"
            type="search"
          />
          <div
            hidden={!searching}
            className="spinner-border spinner-border-sm search-spinner"
            role="status">
            <span className="visually-hidden">Loading...</span>
          </div>
          <button className="btn btn-success search" type="submit">Search</button>
          <Pagination search={search} useSearch={true} page={page} totalPageCount={totalPageCount} />
        </div>
      </Form>

      {results.length > 0 ? (
        <div className="container">
          <div className="row">
            {results.map((item) => (
              <div key={item.slug} id={`meme-${item.slug}`} className="col-sm-6">
                <MemeImage meme={item} user={user} renderLink={true} voteRedirectTo={`${voteRedirectTo}#meme-${item.slug}`} />
              </div>
            ))}
          </div>
        </div>
      ) : (
        <div className="alert alert-secondary" role="alert">
          No memes found.
        </div>
      )}

      <br />
      <Pagination search={search} useSearch={true} page={page} totalPageCount={totalPageCount} />
    </div>
  );
}




