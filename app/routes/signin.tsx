import { AuthDisplay } from "~/components/auth-display";
import { verifySignin } from "~/models/user.server";
import { createUserSession, getUserId } from "~/session.server";
import { EMAIL_MAX_CHARACTER_LENGTH, PASSWORD_MAX_CHARACTER_LENGTH, PASSWORD_MIN_CHARACTER_LENGTH, SimulateNetworkDelay, getTitle, safeRedirect, validateEmail } from "~/utils";

export const meta: MetaFunction = () => [{ title: getTitle('Sign In') }];

export const loader = async ({ request }: LoaderFunctionArgs) => {
  const userId = await getUserId(request);
  if (userId) return redirect("/");

  if (process.env.ENABLE_SIGN_IN !== 'true') {
    return redirect("/");
  }

  const enableSignIn = process.env.ENABLE_SIGN_IN === 'true';
  const enableSignUp = process.env.ENABLE_SIGN_UP === 'true';

  return json({ enableSignIn, enableSignUp });
};

export const action = async ({ request }: ActionFunctionArgs) => {
  await SimulateNetworkDelay();

  if (process.env.ENABLE_SIGN_IN !== 'true') {
    return redirect("/");
  }

  const formData = await request.formData();
  const email = formData.get("email");
  const password = formData.get("password");
  const redirectTo = safeRedirect(formData.get("redirectTo"), "/");
  const remember = formData.get("remember");

  if (!validateEmail(email)) {
    return json(
      { errors: { email: "Please enter a valid email address." } },
      { status: 400 },
    );
  }
  else if (email.length > EMAIL_MAX_CHARACTER_LENGTH) {
    return json(
      { errors: { email: `Email must be no more than ${EMAIL_MAX_CHARACTER_LENGTH} characters.` } },
      { status: 400 },
    );
  }

  if (typeof password !== "string" || password.length === 0) {
    return json(
      { errors: { password: "Password is required." } },
      { status: 400 },
    );
  }
  else if (password.length < PASSWORD_MIN_CHARACTER_LENGTH) {
    return json(
      { errors: { password: `Password must be at least ${PASSWORD_MIN_CHARACTER_LENGTH} characters.` } },
      { status: 400 },
    );
  }
  else if (password.length > PASSWORD_MAX_CHARACTER_LENGTH) {
    return json(
      { errors: { password: `Password must be no more than ${PASSWORD_MAX_CHARACTER_LENGTH} characters.` } },
      { status: 400 },
    );
  }

  const user = await verifySignin(email, password);

  if (!user) {
    return json(
      { errors: { email: "Email or password is invalid.  Please try again.", password: null } },
      { status: 400 },
    );
  }

  return createUserSession({
    redirectTo,
    remember: remember === "on" ? true : false,
    request,
    userId: user.id,
    isNewSignUp: false
  });
};

export default function SignInPage() {
  const { enableSignIn, enableSignUp } = useLoaderData<typeof loader>();
  const actionData = useActionData<typeof action>();

  return (
    <>
      <AuthDisplay action="signin" actionData={actionData} enableSignIn={enableSignIn} enableSignUp={enableSignUp} />
    </>
  );
}
