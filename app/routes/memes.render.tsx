import invariant from "tiny-invariant";
import { getMeme, updateMemeImageData } from "~/models/meme.server";
import { requireUser } from "~/session.server";
import { SimulateNetworkDelay, checkIfAdmin } from "~/utils";

export const action = async ({
  request,
}: ActionFunctionArgs) => {
  await SimulateNetworkDelay();

  const user = await requireUser(request);
  const isAdmin = checkIfAdmin(user);
  
  invariant(isAdmin, 'User must be an admin.');
  
  const formData = await request.formData();
  const data = Object.fromEntries(formData);

  invariant(data.slug, "params.slug is required");
  invariant(data.imageData, "params.imageData is required");

  const meme = await getMeme(data.slug as string);
  invariant(meme, "Meme not found");

  return await updateMemeImageData(meme.slug, data.imageData as string, true, user.id);
};
