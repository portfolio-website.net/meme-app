import invariant from "tiny-invariant";
import { updateMemeVote } from "~/models/meme.server";
import { requireUserId } from "~/session.server";
import { SimulateNetworkDelay, safeRedirect } from "~/utils";

export const action = async ({
  params,
  request,
}: ActionFunctionArgs) => {
  await SimulateNetworkDelay();

  const updatedByUserId = await requireUserId(request);
  const formData = await request.formData();
  const slug = formData.get("slug")?.toString();
  invariant(slug, "Missing slug param");

  const redirectTo = safeRedirect(formData.get("redirectTo"), "/");

  const updateResponse = await updateMemeVote(slug, {
    vote: formData.get("vote") === "true",
    updatedByUserId
  });

  const isUsingJavascript = formData.get("isUsingJavascript") == "true";
  if (isUsingJavascript) {
    return updateResponse;
  }
  else {
    return redirect(redirectTo);
  }
};