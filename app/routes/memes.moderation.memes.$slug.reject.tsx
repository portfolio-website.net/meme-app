import { getMeme, updateMemeModerationAction } from "~/models/meme.server";
import { requireUser } from "~/session.server";
import invariant from "tiny-invariant";
import { COMMENT_MAX_CHARACTER_LENGTH, MAX_LENGTH_FOR_INPUT, MIN_CHARACTER_LENGTH, SimulateNetworkDelay, checkIfAdmin, getMemeImageUrl, getTitle } from "~/utils";

export const meta: MetaFunction = () => [{ title: getTitle('Moderation - Reject Meme') }];

export const validateModerationComment = (comment: string) => {
  if (typeof comment !== "string" || comment.length === 0) {
    return "Comment is required.";
  }
  else if (comment.length < MIN_CHARACTER_LENGTH) {
    return `Comment must be at least ${MIN_CHARACTER_LENGTH} characters long.`;
  }
  else if (comment.length > COMMENT_MAX_CHARACTER_LENGTH) {
    return `Comment must be no more than ${COMMENT_MAX_CHARACTER_LENGTH} characters long.`;
  }
};

export type ModerationCommentErrors = {
  moderationComment: string | undefined;
}

export const loader = async ({ request, params }: LoaderFunctionArgs) => {
  let user = await requireUser(request);
  const isAdmin = checkIfAdmin(user);

  invariant(isAdmin, 'User must be an admin.');

  invariant(params.slug, "params.slug is required");

  const meme = await getMeme(params.slug, user.id);
  
  if (!meme) {
    throw new Response(null, {
      status: 404,
      statusText: "Not Found",
    });
  }

  const url = new URL(request.url);
  let search = url.searchParams.get("search") as string;

  if (search == null) {
    search = '';
  }

  let page = Number(url.searchParams.get("page"));

  if (page == 0) {
    page = 1;
  }

  return json({ meme, search, page });
};

export const action = async ({
  params,
  request,
}: ActionFunctionArgs) => {
  await SimulateNetworkDelay();

  let user = await requireUser(request);
  const isAdmin = checkIfAdmin(user);

  invariant(isAdmin, 'User must be an admin.');

  const formData = await request.formData();
  const data = Object.fromEntries(formData);
  const slug = formData.get("slug")?.toString();
  invariant(slug, "Missing slug param");

  const search = formData.get("search")?.toString();
  const page = formData.get("page")?.toString();

  let formErrors: ModerationCommentErrors = {
    moderationComment: validateModerationComment(data.moderationComment as string)
  };

  if (Object.values(formErrors).some(Boolean)) {
    return json(formErrors);
  }

  await updateMemeModerationAction({
    slug: slug,
    approve: formData.get("approve") === "true",
    moderationComment: data.moderationComment as string,
    updatedByUserId: user.id
  });

  return redirectDocument(`/memes/moderation/memes?search=${search}&page=${page}`);
};

export default function ModerationReject() {
  const { meme, search, page } = useLoaderData<typeof loader>();
  const errors = useActionData<typeof action>();

  const navigation = useNavigation();
  const isSubmitting = Boolean(
    navigation.state === "submitting"
  );

  const isUpdateAction = navigation.formData
    ? navigation.formData.get("approve") === "false" && isSubmitting
    : false;

  const detfaultModerationComment = 'Sorry, the meme could not be approved.';

  const moderationCommentRef = useRef<HTMLTextAreaElement>(null);

  useEffect(() => {
    moderationCommentRef.current?.focus();
  }, [meme.moderationComment]);

  return (
    <div className="container">
      <div className="row g-3">
        <div className="col-md-5">
          <Form method="post">
            <h2>
              Reject Meme
              <br />
              <img className="card-img-top" src={getMemeImageUrl(meme)} alt={`${meme.topText} ${meme.bottomText}`} />
            </h2>
            <div>
              <label htmlFor="moderationComment" className="form-label">Rejection Reason</label>
              <div className="col-sm-10 input-group has-validation">
                <textarea ref={moderationCommentRef} id="moderationComment" className="form-control" name="moderationComment" rows={3} defaultValue={detfaultModerationComment} maxLength={MAX_LENGTH_FOR_INPUT} />
                {errors?.moderationComment ? (
                  <div className="custom-invalid-feedback">{errors.moderationComment}</div>
                ) : null}
              </div>
            </div>
            <div>
              <Link
                to={`/memes/moderation/memes?search=${search}&page=${page}`}
                reloadDocument
                className="btn btn-sm btn-secondary"
              >
                Cancel
              </Link>
              &nbsp;
              <input type="hidden" name="slug" value={meme.slug} />
              <input type="hidden" name="search" value={search} />
              <input type="hidden" name="page" value={page} />
              <input type="hidden" name="approve" value="false" />
              <button type="submit" className="btn btn-sm btn-primary" disabled={isUpdateAction}>
                {isUpdateAction ? (
                  <span className="spinner-border spinner-border-sm" aria-hidden="true"></span>)
                  : null}
                {isUpdateAction ? 'Rejecting...' : "Reject"}
              </button>
            </div>
          </Form>
        </div>
      </div>
    </div>
  );
}