import {
  useEventStream,
} from '@remix-sse/client';
import { MemeImportInfo, MemeSelection } from './import-memes.stream.$page';
import { requireUser } from '~/session.server';
import { checkIfAdmin, getTitle } from '~/utils';
import invariant from 'tiny-invariant';

export const meta: MetaFunction = () => [{ title: getTitle('Import Memes') }];

export const loader = async ({
  request,
}: LoaderFunctionArgs) => {
  const user = await requireUser(request);
  const isAdmin = checkIfAdmin(user);
  
  invariant(isAdmin, 'User must be an admin.');
  
  const url = new URL(request.url);
  let page = Number(url.searchParams.get("page"));

  if (page == 0) {
    page = 1;
  }

  const totalPages = 59;

  return json({ page, totalPages });
};

export default function ImportMemes() {
  let { page, totalPages } = useLoaderData<typeof loader>();

  const memeSelections = useEventStream(`/import-memes/stream/${page}`, {
    channel: 'memeSelections',
    deserialize: (raw) => JSON.parse(raw) as MemeSelection,
  });

  const memeImportInfo = useEventStream(`/import-memes/stream/${page}`, {
    channel: 'memeImportInfo',
    deserialize: (raw) => JSON.parse(raw) as MemeImportInfo,
    returnLatestOnly: true,
  });

  // https://stackoverflow.com/a/75988895
  const debounce = (callback, wait) => {
    let timeoutId = null;
    return (...args) => {
      window.clearTimeout(timeoutId);
      timeoutId = window.setTimeout(() => {
        callback(...args);
      }, wait);
    };
  }

  const updatePageLocation = debounce(() => {
    location.href = `import-memes?page=${page + 1}`;
  }, 250);

  useEffect(() => {
    if (page < totalPages
      && memeImportInfo?.status == 'Complete') {
      updatePageLocation();
    }
  });

  page = memeImportInfo?.page ?? page;
  totalPages = memeImportInfo?.totalPages ?? totalPages;
  const percentComplete = (page / totalPages) * 100;

  const currentMemeSelection = memeSelections != null && memeSelections.length > 0
    ? memeSelections[memeSelections.length - 1]
    : null;

  return (
    <div className="container">
      {page == totalPages && memeImportInfo?.status == 'Complete' ? (
        <div className="alert alert-success" role="alert">
          Processing Complete - All memes imported! ✨
        </div>
      ) : (
        <div className="alert alert-primary" role="alert">
          <div className="progress" role="progressbar" aria-label="Animated striped example" aria-valuenow={percentComplete} aria-valuemin={0} aria-valuemax={100}>
            <div className="progress-bar progress-bar-striped progress-bar-animated" style={{ width: `${percentComplete}%` }}>{' '}{Math.round(percentComplete)}%</div>
          </div>
          Importing <strong>{currentMemeSelection != null ? currentMemeSelection.name : ''}</strong> from page {page}... {' '}
          <span className="spinner-border spinner-border-sm" aria-hidden="true"></span>
          {currentMemeSelection?.imageLink != null ? (
            <>
              <br />
              <div style={{ minHeight: '150px' }}>
                <img src={currentMemeSelection.imageLink} style={{ maxWidth: '150px' }} />
              </div>
            </>
          ) : null}
          <hr />
          Meme pages remaining: {totalPages - (page)}
        </div>
      )}

      <div className="container">
        <div className="row">
          {memeSelections != null ? memeSelections.map((meme) => (
            <div key={meme.name} className="col-sm-2">
              <div className="card">
                <img className="card-img-top small-img" src={meme.imageLink} />
                <div className="card-body">
                  {meme.name}
                </div>
              </div>
            </div>
          )) : null}
        </div>
      </div>
    </div>
  );
}

