import { requireUser } from "~/session.server";
import { getTitle, useOptionalUser, } from "~/utils";

export const meta: MetaFunction = () => [{ title: getTitle('Welcome') }];

export const loader = async ({
  request,
}: LoaderFunctionArgs) => {
  await requireUser(request);

  return null;
};

export default function SignUpWelcome() {
  const user = useOptionalUser();
  return (
    <div className="container">
      <div className="row g-3">
        <div className="col-md-10">
          <p>
            Welcome to the Meme App! &nbsp;
            <img src="/meme-app.png" alt="Meme App" />
          </p>
          <p>
            Your <strong>{user.email}</strong> account has been assigned the username <strong>{user.username}</strong>.
          </p>
          <p>
            At any time, you may{" "}
            <Link to="/profile">
              update your username on your profile page
            </Link>
            .
          </p>
          <p>
            <Link
              className="btn btn-primary"
              to="/"
            >
              Continue to the memes
            </Link>
          </p>
        </div>
      </div>
    </div>
  );
}
