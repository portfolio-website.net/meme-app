import invariant from "tiny-invariant";
import { getUserByEmail, getUserById, updateUserEmail, updateUserUsername, updateUserPassword, getUserByUsername, UserWithoutPassword, verifySignin } from "~/models/user.server";
import { commitSession, getSession, requireUser, setInterstitialMessage } from "~/session.server";
import { EMAIL_MAX_CHARACTER_LENGTH, MAX_LENGTH_FOR_INPUT, PASSWORD_MAX_CHARACTER_LENGTH, PASSWORD_MIN_CHARACTER_LENGTH, SimulateNetworkDelay, USERNAME_MAX_CHARACTER_LENGTH, USERNAME_MIN_CHARACTER_LENGTH, checkIfAdmin, getTitle, useUser, validateEmail } from "~/utils";

export const meta: MetaFunction = () => [{ title: getTitle('Profile') }];

export const loader = async ({ request }: LoaderFunctionArgs) => {
  let user: UserWithoutPassword | null = await requireUser(request);
  const isAdmin = checkIfAdmin(user);

  const url = new URL(request.url);
  const id = url.searchParams.get("id") as string;

  if (id != null && isAdmin) {
    user = await getUserById(id);

    if (!user) {
      throw new Response(null, {
        status: 404,
        statusText: "Not Found",
      });
    }
  }

  return json({ userId: user?.id, user });
};

export const action = async ({ request }: ActionFunctionArgs) => {
  await SimulateNetworkDelay();

  const session = await getSession(request);
  let user = await requireUser(request);
  const isAdmin = checkIfAdmin(user);

  const formData = await request.formData();
  const id = formData.get("id");

  invariant(id, `User ID is required`);

  const email = formData.get("email");
  const username = formData.get("username");
  const currentPassword = formData.get("currentPassword");
  const password = formData.get("password");
  const confirmPassword = formData.get("confirmPassword");

  const existingUser = await getUserById(id as string);
  if (!existingUser) {
    return json(
      {
        errors: {
          id: "The user does not exist."
        },
      },
      { status: 400 },
    );
  }

  const url = new URL(request.url);
  const idParameter = url.searchParams.get("id") as string;

  let currentPageUrl = '/profile';
  if (idParameter != null) {
    currentPageUrl = currentPageUrl + `?id=${existingUser.id}`;
  }

  const isOwnAccount = user.id == id;
  invariant(user.id == id || (user.id != id && isAdmin), `Profile not owned by user`);

  if (email != null) {

    if (!validateEmail(email)) {
      return json(
        { errors: { email: "Please enter a valid email address." } },
        { status: 400 },
      );
    }
    else if (email.length > EMAIL_MAX_CHARACTER_LENGTH) {
      return json(
        { errors: { email: `Email must be no more than ${EMAIL_MAX_CHARACTER_LENGTH} characters.` } },
        { status: 400 },
      );
    }

    const existingUserWithEmail = await getUserByEmail(email);
    if (existingUserWithEmail && existingUserWithEmail.id != id) {
      return json(
        {
          errors: {
            email: "A user already exists with this email address.",
          },
        },
        { status: 400 },
      );
    }

    await updateUserEmail(id as string, email, user.id);

    setInterstitialMessage(session, 'Email successfully updated.', currentPageUrl);
    return redirect('/profile/updated', {
      headers: { 'Set-Cookie': await commitSession(session) },
    });
  }
  else if (username != null) {

    if (typeof username !== "string" || username.length === 0) {
      return json(
        { errors: { username: "Username is required." } },
        { status: 400 },
      );
    }
    else if (username.length < USERNAME_MIN_CHARACTER_LENGTH) {
      return json(
        { errors: { username: `Username must be at least ${USERNAME_MIN_CHARACTER_LENGTH} characters.` } },
        { status: 400 },
      );
    }
    else if (username.length > USERNAME_MAX_CHARACTER_LENGTH) {
      return json(
        { errors: { username: `Username must be no more than ${USERNAME_MAX_CHARACTER_LENGTH} characters.` } },
        { status: 400 },
      );
    }

    const existingUserWithUsername = await getUserByUsername(username);
    if (existingUserWithUsername && existingUserWithUsername.id != id) {
      return json(
        {
          errors: {
            username: "A user already exists with this username.",
          },
        },
        { status: 400 },
      );
    }

    await updateUserUsername(id as string, username as string, user.id as string);

    setInterstitialMessage(session, 'Username successfully updated.', currentPageUrl);
    return redirect('/profile/updated', {
      headers: { 'Set-Cookie': await commitSession(session) },
    });
  }
  else if (password != null) {

    if (isOwnAccount) {
      const verifiedUser = await verifySignin(user.email, currentPassword as string);
      if (!verifiedUser) {
        return json(
          { errors: { currentPassword: "Current Password is invalid.  Please try again." } },
          { status: 400 },
        );
      }
    }

    if (typeof password !== "string" || password.length === 0) {
      return json(
        { errors: { password: "Password is required." } },
        { status: 400 },
      );
    }
    else if (password.length < PASSWORD_MIN_CHARACTER_LENGTH) {
      return json(
        { errors: { password: `Password must be at least ${PASSWORD_MIN_CHARACTER_LENGTH} characters.` } },
        { status: 400 },
      );
    }
    else if (password.length > PASSWORD_MAX_CHARACTER_LENGTH) {
      return json(
        { errors: { password: `Password must be no more than ${PASSWORD_MAX_CHARACTER_LENGTH} characters.` } },
        { status: 400 },
      );
    }
    else if (password != confirmPassword) {
      return json(
        { errors: { confirmPassword: "Confirm Password does not match." } },
        { status: 400 },
      );
    }

    await updateUserPassword(id as string, password, user.id);

    setInterstitialMessage(session, 'Password successfully updated.', currentPageUrl);
    return redirect('/profile/updated', {
      headers: { 'Set-Cookie': await commitSession(session) },
    });
  }
};

export default function Profile() {
  const actionData = useActionData<typeof action>();
  const { user } = useLoaderData<typeof loader>();

  let currentUser = useUser();
  const isAdmin = checkIfAdmin(currentUser);

  const isOwnAccount = user?.id == currentUser.id;
  invariant(user?.id == currentUser.id || (user?.id != currentUser.id && isAdmin), `Profile not owned by user`);

  const navigation = useNavigation();
  const isSubmitting = Boolean(
    navigation.state === "submitting"
  );

  const isEmailUpdate = navigation.formData
    ? navigation.formData.get("emailUpdate") === "true" && isSubmitting
    : false;

  const isUsernameUpdate = navigation.formData
    ? navigation.formData.get("usernameUpdate") === "true" && isSubmitting
    : false;

  const isPasswordUpdate = navigation.formData
    ? navigation.formData.get("passwordUpdate") === "true" && isSubmitting
    : false;

  return (
    <div className="container">
      <div className="row g-3">
        <div className="col-md-5">
          <div className="accordion">
            <div className="accordion-item">
              <h2 className="accordion-header">
                <button className="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panel1" aria-expanded="true" aria-controls="panel1">
                  Email
                </button>
              </h2>
              <div id="panel1" className="accordion-collapse collapse show">
                <div className="accordion-body">

                  <Form method="post">
                    <div className="mb-3">
                      <label htmlFor="email" className="form-label">Email Address</label>
                      <input type="email" className="form-control" id="email" name="email" defaultValue={user?.email} maxLength={MAX_LENGTH_FOR_INPUT} />
                      {actionData?.errors?.email ? (
                        <div className="custom-invalid-feedback">
                          {actionData.errors.email}
                        </div>
                      ) : null}
                    </div>
                    <input type="hidden" name="id" value={user?.id} />
                    <input type="hidden" name="emailUpdate" value="true" />
                    <button type="submit" className="btn btn-primary" disabled={isEmailUpdate}>
                      {isEmailUpdate ? (
                        <span className="spinner-border spinner-border-sm" aria-hidden="true"></span>)
                        : null}
                      {isEmailUpdate ? 'Updating Email...' : "Update Email"}
                    </button>
                  </Form>

                </div>
              </div>
            </div>
            <div className="accordion-item">
              <h2 className="accordion-header">
                <button className="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panel2" aria-expanded="true" aria-controls="panel2">
                  Username
                </button>
              </h2>
              <div id="panel2" className="accordion-collapse collapse show">
                <div className="accordion-body">

                  <Form method="post">
                    <div className="mb-3">
                      <label htmlFor="username" className="form-label">Username</label>
                      <input type="text" className="form-control" id="username" name="username" defaultValue={user?.username} maxLength={MAX_LENGTH_FOR_INPUT} />
                      {actionData?.errors?.username ? (
                        <div className="custom-invalid-feedback">
                          {actionData.errors.username}
                        </div>
                      ) : null}
                    </div>
                    <input type="hidden" name="id" value={user?.id} />
                    <input type="hidden" name="usernameUpdate" value="true" />
                    <button type="submit" className="btn btn-primary" disabled={isUsernameUpdate}>
                      {isUsernameUpdate ? (
                        <span className="spinner-border spinner-border-sm" aria-hidden="true"></span>)
                        : null}
                      {isUsernameUpdate ? 'Updating Username...' : "Update Username"}
                    </button>
                  </Form>

                </div>
              </div>
            </div>
            <div className="accordion-item">
              <h2 className="accordion-header">
                <button className="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panel3" aria-expanded="true" aria-controls="panel3">
                  Password
                </button>
              </h2>
              <div id="panel3" className="accordion-collapse collapse show">
                <div className="accordion-body">

                  <Form method="post">
                    {isOwnAccount ? (
                      <div className="mb-3">
                        <label htmlFor="currentPassword" className="form-label">Current Password</label>
                        <input type="password" className="form-control" id="currentPassword" name="currentPassword" maxLength={MAX_LENGTH_FOR_INPUT} />
                        {actionData?.errors?.currentPassword ? (
                          <div className="custom-invalid-feedback">
                            {actionData.errors.currentPassword}
                          </div>
                        ) : null}
                      </div>
                    ) : null}
                    <div className="mb-3">
                      <label htmlFor="password" className="form-label">New Password</label>
                      <input type="password" className="form-control" id="password" name="password" maxLength={MAX_LENGTH_FOR_INPUT} />
                      {actionData?.errors?.password ? (
                        <div className="custom-invalid-feedback">
                          {actionData.errors.password}
                        </div>
                      ) : null}
                    </div>
                    <div className="mb-3">
                      <label htmlFor="confirmPassword" className="form-label">Confirm Password</label>
                      <input type="password" className="form-control" id="confirmPassword" name="confirmPassword" maxLength={MAX_LENGTH_FOR_INPUT} />
                      {actionData?.errors?.confirmPassword ? (
                        <div className="custom-invalid-feedback">
                          {actionData.errors.confirmPassword}
                        </div>
                      ) : null}
                    </div>
                    <input type="hidden" name="id" value={user?.id} />
                    <input type="hidden" name="passwordUpdate" value="true" />
                    <button type="submit" className="btn btn-primary" disabled={isPasswordUpdate}>
                      {isPasswordUpdate ? (
                        <span className="spinner-border spinner-border-sm" aria-hidden="true"></span>)
                        : null}
                      {isPasswordUpdate ? 'Updating Password...' : "Update Password"}
                    </button>
                  </Form>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  );
}

