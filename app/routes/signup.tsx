import { AuthDisplay } from "~/components/auth-display";
import { createUser, getUserByEmail } from "~/models/user.server";
import { createUserSession, getUserId } from "~/session.server";
import { EMAIL_MAX_CHARACTER_LENGTH, PASSWORD_MAX_CHARACTER_LENGTH, PASSWORD_MIN_CHARACTER_LENGTH, SimulateNetworkDelay, getTitle, isValidEmailDomain, safeRedirect, validateEmail } from "~/utils";

export const meta: MetaFunction = () => [{ title: getTitle('Sign Up') }];

export const loader = async ({ request }: LoaderFunctionArgs) => {
  const userId = await getUserId(request);
  if (userId) return redirect("/");

  if (process.env.ENABLE_SIGN_UP !== 'true') {
    return redirect("/");
  }

  const enableSignIn = process.env.ENABLE_SIGN_IN === 'true';
  const enableSignUp = process.env.ENABLE_SIGN_UP === 'true';

  return json({ enableSignIn, enableSignUp });
};

export const action = async ({ request }: ActionFunctionArgs) => {
  await SimulateNetworkDelay();

  if (process.env.ENABLE_SIGN_UP !== 'true') {
    return redirect("/");
  }

  const formData = await request.formData();
  const email = formData.get("email");
  const password = formData.get("password");
  const redirectTo = safeRedirect(formData.get("redirectTo"), "/");

  if (!validateEmail(email)) {
    return json(
      { errors: { email: "Please enter a valid email address." } },
      { status: 400 },
    );
  }
  else if (email.length > EMAIL_MAX_CHARACTER_LENGTH) {
    return json(
      { errors: { email: `Email must be no more than ${EMAIL_MAX_CHARACTER_LENGTH} characters.` } },
      { status: 400 },
    );
  }

  if (typeof password !== "string" || password.length === 0) {
    return json(
      { errors: { password: "Password is required." } },
      { status: 400 },
    );
  }
  else if (password.length < PASSWORD_MIN_CHARACTER_LENGTH) {
    return json(
      { errors: { password: `Password must be at least ${PASSWORD_MIN_CHARACTER_LENGTH} characters.` } },
      { status: 400 },
    );
  }
  else if (password.length > PASSWORD_MAX_CHARACTER_LENGTH) {
    return json(
      { errors: { password: `Password must be no more than ${PASSWORD_MAX_CHARACTER_LENGTH} characters.` } },
      { status: 400 },
    );
  }

  if (!isValidEmailDomain(email)) {
    return json(
      {
        errors: {
          email: "The email cannot be registered at this time."
        },
      },
      { status: 400 },
    );
  }

  const existingUser = await getUserByEmail(email);
  if (existingUser) {
    return json(
      {
        errors: {
          email: "A user already exists with this email."
        },
      },
      { status: 400 },
    );
  }

  const user = await createUser(email, password);

  return createUserSession({
    redirectTo,
    remember: false,
    request,
    userId: user.id,
    isNewSignUp: true
  });
};

export default function SignUp() {
  const { enableSignIn, enableSignUp } = useLoaderData<typeof loader>();
  const actionData = useActionData<typeof action>();

  return (
    <>
      <AuthDisplay action="signup" actionData={actionData} enableSignIn={enableSignIn} enableSignUp={enableSignUp} />
    </>
  );
}
