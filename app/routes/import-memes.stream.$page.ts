import { EventStream } from '@remix-sse/server';
import axios from 'axios';
import cheerio from 'cheerio';
import fs from 'fs';
import { checkIfAdmin, convertTitleToSlug } from "~/utils";
import invariant from 'tiny-invariant';
import { createCanvas, loadImage } from 'canvas';
import { requireUser } from '~/session.server';

export type MemeImportInfo = {
  status: string;
  page: number;
  totalPages: number;
};

export type MemeSelection = {
  name: string;
  imageLink: string;
};

export type ApiMemeSelection = {
  name: string;
  slug: string;
};

// https://stackoverflow.com/a/1186465
function gcd(a, b) {
  return (b == 0) ? a : gcd(b, a % b);
}

export async function downloadMemeImage(
  url: string
) {
  const background = await loadImage(url);
  const width = background.width;
  const height = background.height;
  var r = gcd(width, height);

  const aspectRatio = `${width / r}:${height / r}`;
  console.log(`${url}: ${aspectRatio}`);

  if ((height / r) / (width / r) > 1) {
    return null;
  }

  const canvas = createCanvas(width, height);
  const ctx = canvas.getContext('2d');

  ctx.drawImage(background, 0, 0, width, height);

  const image = canvas.toBuffer('image/jpeg');
  return image;
}

export const loader: LoaderFunction = ({ params, request }) => {
  return new EventStream(request, async (send) => {
    const user = await requireUser(request);
    const isAdmin = checkIfAdmin(user);

    invariant(isAdmin, 'User must be an admin.');

    invariant(params.page, "params.page is required");
    let page = Number(params.page);

    if (page == 0) {
      page = 1;
    }
    const totalPages = 59;
    const baseUrl = 'https://imgflip.com';
    const memeTemplatesBaseUrl = `${baseUrl}/memetemplates`;

    let memeImportInfo: MemeImportInfo = {
      page: page,
      totalPages: totalPages,
      status: 'Processing'
    };

    send(
      JSON.stringify(memeImportInfo), {
      channel: 'memeImportInfo'
    });

    const requestUrl = `${memeTemplatesBaseUrl}?page=${page}`;

    const memeSelectionsTempFileName = 'meme-selections-temp.json';
    const apiMemeSelectionsTempFileName = 'api-meme-selections-temp.json';
    const memeSelectionsFinalFileName = 'imported-meme-selections.json';
    let memeSelections: any[] = [];

    if (page > 1) {
      let jsonString = fs.readFileSync(memeSelectionsTempFileName, 'utf8');
      memeSelections = JSON.parse(jsonString);
    }

    let apiMemeSelections: any[] = [];

    if (page == 1) {
      apiMemeSelections = await getApiMemeSelections();

      apiMemeSelections = apiMemeSelections.
        filter((obj, index, self) => index ===
          self.findIndex((o) => convertTitleToSlug(o.name) === convertTitleToSlug(obj.name))
        );

      fs.writeFileSync(apiMemeSelectionsTempFileName, JSON.stringify(apiMemeSelections), (err) => {
        if (err) console.log('Error writing file:', err);
      });
    }
    else {
      let jsonString = fs.readFileSync(apiMemeSelectionsTempFileName, 'utf8');
      apiMemeSelections = JSON.parse(jsonString);
    }

    const memeSelectionsFromUrl = await getMemeSelectionsFromURL(requestUrl);

    async function getMemeSelectionsFromURL(url) {
      let memeSelections = [];
      let httpResponse = await axios.get(url);

      let $ = cheerio.load(httpResponse.data);
      let linkObjects = $('a'); // get all hyperlinks

      for (const element of linkObjects) {
        if ($(element).text().trim() != ''
          && $(element).attr('href')?.startsWith('/meme/')) {

          const blankTemplateLink = await getBlankTemplateLink(baseUrl + $(element).attr('href'));
          if (blankTemplateLink != null) {
            const blankTemplateImageLink = await getBlankTemplateImageLink(baseUrl + blankTemplateLink);
            if (blankTemplateImageLink != null) {

              let memeSelection: MemeSelection = {
                name: $(element).text(), // get the text
                imageLink: blankTemplateImageLink.startsWith('//')
                  ? 'https:' + blankTemplateImageLink
                  : baseUrl + blankTemplateImageLink, // get the href attribute
              }

              if (isNameInApiMemeSelections(memeSelection.name, apiMemeSelections)) {
                const image = await downloadMemeImage(memeSelection.imageLink);
                if (image != null) {
                  const slug = convertTitleToSlug(memeSelection.name);
                  const imageFileName = `public/images/memes/${slug}.jpg`;
                  fs.writeFileSync(imageFileName, image, (err) => {
                    if (err) console.log('Error writing file:', err);
                  });

                  memeSelection.imageLink = `/images/memes/${slug}.jpg`

                  console.log(memeSelection.imageLink);

                  await new Promise((res) => setTimeout(res, 500));

                  send(
                    JSON.stringify(memeSelection), {
                    channel: 'memeSelections'
                  }
                  );

                  memeSelections.push(memeSelection);
                }
              }
            }
          }
        }
      }

      return memeSelections;
    }

    for (const element of memeSelectionsFromUrl) {
      memeSelections.push({ name: element.name });
    }

    memeSelections = memeSelections.
      filter((obj, index, self) => index ===
        self.findIndex((o) => convertTitleToSlug(o.name) === convertTitleToSlug(obj.name))
      );

    fs.writeFileSync(memeSelectionsTempFileName, JSON.stringify(memeSelections), (err) => {
      if (err) console.log('Error writing file:', err);
    });

    if (page == totalPages) {
      let jsonString = fs.readFileSync(memeSelectionsTempFileName, 'utf8');
      memeSelections = JSON.parse(jsonString);

      let apiMemeSelections: any[] = [];
      let filteredApiMemeSelections: any[] = [];
      jsonString = fs.readFileSync(apiMemeSelectionsTempFileName, 'utf8');
      apiMemeSelections = JSON.parse(jsonString);

      for (const item of apiMemeSelections) {
        if (memeSelections.filter(x => convertTitleToSlug(x.name) == convertTitleToSlug(item.name)).length > 0) {
          filteredApiMemeSelections.push(
            {
              name: item.name,
              slug: item.slug
            }
          )
        }
      }

      fs.writeFileSync(memeSelectionsFinalFileName, JSON.stringify(filteredApiMemeSelections), (err) => {
        if (err) console.log('Error writing file:', err);
      });
    }

    memeImportInfo = {
      page: page,
      totalPages: totalPages,
      status: 'Complete'
    };

    send(
      JSON.stringify(memeImportInfo), {
      channel: 'memeImportInfo'
    });
  });
};

async function getBlankTemplateLink(url) {
  let httpResponse = await axios.get(url);

  let $ = cheerio.load(httpResponse.data);
  let linkObjects = $('a'); // get all hyperlinks

  for (const element of linkObjects) {
    if ($(element).text().trim().startsWith('Blank Template')) {
      const blankTemplateLink = $(element).attr('href');
      return blankTemplateLink;
    }
  }

  return null;
}

async function getBlankTemplateImageLink(url) {
  let httpResponse = await axios.get(url);

  let $ = cheerio.load(httpResponse.data);
  let linkObjects = $('img'); // get all hyperlinks

  for (const element of linkObjects) {
    if ($(element).attr('id') == 'mtm-img') {
      const blankTemplateImageLink = $(element).attr('src');
      return blankTemplateImageLink;
    }
  }

  return null;
}

async function getApiMemeSelections() {
  const totalPages = 29;
  const baseUrl = 'https://apimeme.com';

  let memeSelections = [];
  for (let page = 1; page <= totalPages; page++) {
    let httpResponse = await axios.get(`${baseUrl}?page=${page}`);

    let $ = cheerio.load(httpResponse.data);
    let linkObjects = $('a'); // get all hyperlinks

    for (const element of linkObjects) {
      if ($(element).text().trim() != ''
        && $(element).attr('href')?.startsWith('/create/')) {

        let imageLink = $(element).attr('href');
        let imageEndIndex = imageLink?.indexOf('?');
        if (imageEndIndex == undefined || imageEndIndex <= 0) {
          imageEndIndex = imageLink?.length;
        }
        const slug = imageLink?.substring(0, imageEndIndex).replace('/create/', '');

        let memeSelection: ApiMemeSelection = {
          name: $(element).text().trim(), // get the text
          slug: slug as string
        }

        console.log(memeSelection.slug);
        memeSelections.push(memeSelection);
      }
    }
  }

  return memeSelections;
}

function isNameInApiMemeSelections(
  name: string,
  apiMemeSelections: any[]) {

  return apiMemeSelections.filter(x => convertTitleToSlug(x.name) == convertTitleToSlug(name)).length > 0;
}