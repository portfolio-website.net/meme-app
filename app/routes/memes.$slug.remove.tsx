import invariant from "tiny-invariant";
import { getMeme } from "~/models/meme.server";
import { requireUser } from "~/session.server";
import { checkIfEditable, getMemeImageUrl, getTitle } from "~/utils";

export const meta: MetaFunction = () => [{ title: getTitle('Remove Meme') }];

export const loader = async ({
  params,
  request
}: LoaderFunctionArgs) => {
  const user = await requireUser(request);
  invariant(params.slug, "params.slug is required");

  const meme = await getMeme(params.slug, user.id);
  
  if (!meme) {
    throw new Response(null, {
      status: 404,
      statusText: "Not Found",
    });
  }

  const canEdit = await checkIfEditable(user, false, meme?.active, meme?.createdByUserId);
  invariant(canEdit, 'Cannot remove meme');

  return json({ meme });
};

export default function RemoveMeme() {
  const { meme } = useLoaderData<typeof loader>();
  const navigation = useNavigation();
  const isSubmitting = Boolean(
    navigation.state === "submitting"
  );

  let isRemoveAction = navigation.formData
    ? navigation.formData?.get("remove") === "true" && isSubmitting
    : false;

  return (
    <div className="container">
      <div className="row g-3">
        <div className="col-md-5">
          <div className="alert alert-warning" role="alert">
            Are you sure you want to remove this meme?
            <br />
            <img className="card-img-top" src={getMemeImageUrl(meme)} alt={`${meme.topText} ${meme.bottomText}`} />
          </div>
          <Form method="post"
            action={`/memes/${meme.slug}/edit`}>
            <Link
              to={`/memes/${meme.slug}`}
              className="btn btn-secondary"
            >
              Back
            </Link>
            &nbsp;
            <input type="hidden" name="id" value={meme.id} />
            <button type="submit" className="btn btn-danger" name="remove" value="true" disabled={isRemoveAction}>
              {isRemoveAction ? (
                <span className="spinner-border spinner-border-sm" aria-hidden="true"></span>)
                : null}
              {isRemoveAction ? 'Removing Meme...' : "Remove Meme"}
            </button>
          </Form>
        </div>
      </div>
    </div>
  );
}
