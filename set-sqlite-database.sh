#!/bin/bash

# Configure the Prisma SQLite provider.
sed -i -e 's/provider = "postgresql"/provider = "sqlite"/' ./prisma/schema.prisma

# Comment out the case-insensitive mode for the SQLite provider.
# Reference: https://www.prisma.io/docs/orm/prisma-client/queries/case-sensitivity#sqlite-provider
sed -i -e "s/mode: 'insensitive'/\/\/mode: 'insensitive'/g" ./app/models/comment.server.ts
sed -i -e "s/mode: 'insensitive'/\/\/mode: 'insensitive'/g" ./app/models/meme.server.ts
sed -i -e "s/mode: 'insensitive'/\/\/mode: 'insensitive'/g" ./app/models/user.server.ts